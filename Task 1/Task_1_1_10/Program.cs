﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_8 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[,] ar = new int[5,5];
            Random rand = new Random();

            
            for(int i = 0; i<5; ++i)
            {
                for(int j = 0; j<5; ++j)
                {
                    ar[i,j] = rand.Next(5);
                }
            }

            for (int i = 0; i < 5; ++i, Console.WriteLine())
            {
                for (int j = 0; j < 5; ++j)
                {
                    Console.Write($"{ar[i,j]} ");
                }
            }

            int sum = 0;
            for (int i = 0; i < 5; ++i)
            {
                for(int j = 0; j<5; ++j)
                {
                    if ((i + j) % 2 == 0) sum += ar[i, j];
                }
            }

            Console.WriteLine($"Сумма элементов массива, стоящих на чётных позициях: {sum}");

        }
    }
}
