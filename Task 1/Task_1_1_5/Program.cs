﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_5 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int sum=0;
            for (int i = 1; i < 1000; ++i)
            {
                if (i % 3 == 0 || i % 5 == 0) sum += i;
            }
            Console.WriteLine(sum);

        }
    }
}
