﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_7 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Random rand = new Random();
            int[] array = new int[10];
            for (int i=0; i<10; ++i) array[i] = rand.Next(-100, 100);

            int min = array[0];
            int max = array[0];

            foreach(int i in array)
            {
                if (i < min) min = i;
                if (i > max) max = i;
            }
            
            for(int i=0; i<10; ++i)
            {
                for(int j=0; j<9; ++j)
                {
                    if(array[j]>array[j+1])
                    {
                        int t = array[j];
                        array[j] = array[j+1];
                        array[j+1] = t;
                    }
                }
            }
            Console.WriteLine($"Минимальный эл. массива: {min}");
            Console.WriteLine($"Максимальный эл. массива: {max}");
            foreach (int i in array) Console.Write($"{i} ");

        }
    }
}
