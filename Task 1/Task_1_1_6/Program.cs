﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_6 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void getStyle(List<string> style)
        {
            Console.Write("Параметры надписи: ");
            if(style.Count>0)
            {
                foreach (string s in style) Console.Write($"{s} ");
                Console.WriteLine();
            }
            else Console.WriteLine("None");
        }

        public static void setStyle(List<string> style)
        {
            Console.WriteLine("Введите:\n\t1: bold\n\t2: italic\n\t3: underline");
            int x = int.Parse(Console.ReadLine());
            switch (x)
            {
                case 1:
                    if (style.Contains("Bold")) style.Remove("Bold");
                    else style.Add("Bold");
                    break;
                case 2:
                    if (style.Contains("Italic")) style.Remove("Italic");
                    else style.Add("Italic");
                    break;
                case 3:
                    if (style.Contains("Underline")) style.Remove("Underline");
                    else style.Add("Underline");
                    break;
                default:
                    break;
            }
        }

        public static void Main(string[] args)
        {
            List<string> style = new List<string>();
            getStyle(style);
            setStyle(style);
            getStyle(style);
            setStyle(style);
            getStyle(style);
            setStyle(style);
            getStyle(style);
        }
    }
}
