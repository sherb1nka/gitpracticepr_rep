﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_8 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[] ar = new int[10];
            Random rand = new Random();

            for (int i=0; i<10; ++i)
            {
                ar[i] = rand.Next(-10, 10);
            }

            int sum=0;

            foreach(int i in ar) Console.Write($"{i} ");

            foreach (int i in ar)
                if (i > 0) sum += i;

            Console.WriteLine($"\n{sum}");

        }
    }
}
