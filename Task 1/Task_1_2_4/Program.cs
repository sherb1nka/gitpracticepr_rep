﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_1_2_1 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            StringBuilder res = new StringBuilder(s);

            string splits = ".?!";
            int upper_letter_i = 0;
            for(int i=0; i<res.Length; ++i)
            {
                if(splits.Contains(res[i]))
                {
                   res[upper_letter_i] = Char.ToUpper(res[upper_letter_i]);
                   if (i + 2 < res.Length) upper_letter_i=i+2;
                }
            }

            Console.WriteLine(res);
        }
    }
}
