﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_1_2_1 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите строку 1:");
            string s1 = Console.ReadLine();
            Console.WriteLine("Введите строку 2:");
            string s2 = Console.ReadLine();
            
            StringBuilder res = new StringBuilder(s1);

            for(int i = 0; i<res.Length; ++i)
            {
                if(s2.Contains(res[i]))
                {
                    res.Insert(i+1, res[i++]);
                }
            }

            Console.WriteLine($"Результат: {res}");

        }
    }
}
