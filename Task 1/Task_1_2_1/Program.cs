﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_2_1 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            List<int> counts = new List<int>();

            char[] splits = { ' ', ',', '.', '?', ':', '!', '-', ';'};

            foreach (string word in s.Split(splits, StringSplitOptions.RemoveEmptyEntries))
            {
                counts.Add(word.Count());
            }

            double sum = 0;
            foreach (int count in counts) sum += count;

            Console.WriteLine($"Средняя длина слов: {sum/counts.Count()}");

            //Я ничё не округляю

        }
    }
}

