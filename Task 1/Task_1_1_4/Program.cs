﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_4 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Input N: ");
            int N = int.Parse(Console.ReadLine());

            for (int i = 1; i <= N; ++i)
            {
                for (int k = 1; k <= i; ++k, Console.WriteLine())
                {
                    for (int j = N - k; j > 0; --j) Console.Write(" ");
                    for (int j = 1; j < k * 2; j++) Console.Write("*");
                }
            }

        }
    }
}
