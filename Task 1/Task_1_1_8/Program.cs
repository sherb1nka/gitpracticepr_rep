﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_8 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[,,] ar = new int[5, 5, 5];

            Random rand = new Random();
            for (int i = 0; i < 5; ++i) 
                for(int j=0; j<5; ++j)
                    for(int k=0; k<5; ++k)
                        ar[i,j,k] = rand.Next(-100, 100);

            for (int i = 0; i < 5; ++i)
                for (int j = 0; j < 5; ++j)
                    for (int k = 0; k < 5; ++k)
                        if(ar[i,j,k]>0) ar[i,j,k]=0;  
            
            foreach(int i in ar) Console.Write($"{i} ");

        }
    }
}
