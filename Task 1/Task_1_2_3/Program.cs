﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_1_2_1 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите строку");
            string s = Console.ReadLine();
            List<int> counts = new List<int>();

            char[] splits = { ' ', ',', '.', '?', ':', '!', '-', ';' };

            int sum=0;

            foreach (string word in s.Split(splits, StringSplitOptions.RemoveEmptyEntries))
            {
                if (Char.IsLower(word[0])) sum++;
            }

            Console.WriteLine($"Колличество слов с маленькой буквы: {sum}");

        }
    }
}
