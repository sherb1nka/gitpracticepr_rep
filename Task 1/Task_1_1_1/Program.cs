﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_1 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Input a: ");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Input b: ");
            int b = int.Parse(Console.ReadLine());
            if (a > 0 && b > 0)
            {
                Console.WriteLine($"Rectangle's area = {a*b}");
            }
            else Console.WriteLine("Incorrect values.");
        }
    }
}