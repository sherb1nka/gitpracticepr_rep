﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_1_1_2 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Input N: ");
            int N = int.Parse(Console.ReadLine());

            for(int i = 1; i<=N; ++i, Console.WriteLine())
            {
                for (int j = 1; j <= i; ++j) Console.Write("*");
            }
            
        }
    }
}